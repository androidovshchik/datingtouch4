@file:Suppress("SpellCheckingInspection")

import com.android.build.gradle.internal.api.BaseVariantOutputImpl
import org.jetbrains.kotlin.config.KotlinCompilerVersion
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
}

android {
    compileSdkVersion(29)
    defaultConfig {
        minSdkVersion(19)
        targetSdkVersion(29)
        versionCode = 1
        versionName = "1.0"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            isShrinkResources = true
            signingConfig = signingConfigs.getByName("debug")
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }
    flavorDimensions("default")
    productFlavors {
        create("ChatGirls") {
            applicationId = "girlschat.ratsate"
            resValue("string", "app_name", "Chat Girls")
            resValue("string", "app_link", "http://chatww.info/gWCW3d2T")
            resValue("string", "facebook_app_id", "715429345345345")
            buildConfigField("String", "LOGIN", "\"44;588;677;645\"")
            buildConfigField("String", "PASSWORD", "\"43;681;675;735\"")
            buildConfigField("String", "SIGN_IN", "\"292;811;447;854\"")
        }
        create("App2") {
            applicationId = "girlschat.ratsate"
            resValue("string", "app_name", "Chat Girls")
            resValue("string", "app_link", "http://chatww.info/gWCW3d2T")
            resValue("string", "facebook_app_id", "715429345345345")
            buildConfigField("String", "LOGIN", "\"44;588;677;645\"")
            buildConfigField("String", "PASSWORD", "\"43;681;675;735\"")
            buildConfigField("String", "SIGN_IN", "\"292;811;447;854\"")
        }
        create("App3") {
            applicationId = "girlschat.ratsate"
            resValue("string", "app_name", "Chat Girls")
            resValue("string", "app_link", "http://chatww.info/gWCW3d2T")
            resValue("string", "facebook_app_id", "715429345345345")
            buildConfigField("String", "LOGIN", "\"44;588;677;645\"")
            buildConfigField("String", "PASSWORD", "\"43;681;675;735\"")
            buildConfigField("String", "SIGN_IN", "\"292;811;447;854\"")
        }
        create("App4") {
            applicationId = "girlschat.ratsate"
            resValue("string", "app_name", "Chat Girls")
            resValue("string", "app_link", "http://chatww.info/gWCW3d2T")
            resValue("string", "facebook_app_id", "715429345345345")
            buildConfigField("String", "LOGIN", "\"44;588;677;645\"")
            buildConfigField("String", "PASSWORD", "\"43;681;675;735\"")
            buildConfigField("String", "SIGN_IN", "\"292;811;447;854\"")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    applicationVariants.all {
        outputs.forEach { output ->
            (output as BaseVariantOutputImpl).outputFileName = "${applicationId}.apk"
        }
    }
}

repositories {
    maven {
        setUrl("https://jitpack.io")
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(kotlin("stdlib-jdk8", KotlinCompilerVersion.VERSION))
    implementation("org.jetbrains.anko:anko-commons:0.10.8")
    implementation("org.jetbrains.anko:anko-sdk21:0.10.8")
    implementation("androidx.core:core-ktx:1.2.0")
    implementation("androidx.legacy:legacy-support-v13:1.0.0")
    implementation("androidx.viewpager:viewpager:1.0.0")
    implementation("androidx.swiperefreshlayout:swiperefreshlayout:1.0.0")
    implementation("io.coil-kt:coil:0.9.5")
    implementation("com.android.volley:volley:1.1.1")
    implementation("com.github.delight-im:Android-AdvancedWebView:v3.2.0")
    implementation("com.github.MikeOrtiz:TouchImageView:3.0.1")
    implementation("com.facebook.android:facebook-android-sdk:5.15.3")
    implementation("com.jakewharton.timber:timber:4.7.1")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}
