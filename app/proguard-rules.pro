-keepattributes SourceFile,LineNumberTable
-renamesourcefileattribute SourceFile
-repackageclasses

-keep class com.ortiz.touchview.TouchImageView {
  android.graphics.PointF transformCoordTouchToBitmap(...);
  android.graphics.PointF transformCoordBitmapToTouch(...);
}