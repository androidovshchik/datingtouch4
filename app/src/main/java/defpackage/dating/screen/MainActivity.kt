package defpackage.dating.screen

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.view.ViewTreeObserver
import androidx.core.view.isVisible
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import defpackage.dating.R
import im.delight.android.webview.AdvancedWebView
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity

class MainActivity : Activity(), AdvancedWebView.Listener {

    private val scrollChangedListener = ViewTreeObserver.OnScrollChangedListener {
        srl_web.isEnabled = wv_web.scrollY == 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        srl_web.apply {
            setColorSchemeColors(Color.parseColor("#E91E63"))
            setOnRefreshListener {
                wv_web.reload()
                srl_web.isRefreshing = false
            }
        }
        wv_web.setListener(this, this)
        val queue = Volley.newRequestQueue(this)
        val stringRequest = StringRequest(
            Request.Method.GET,
            getString(R.string.app_link),
            Response.Listener {
                if (!isFinishing) {
                    if (it.isNullOrBlank()) {
                        startActivity<LoginActivity>()
                        finish()
                    } else {
                        pb_request.isVisible = false
                        wv_web.loadUrl(it)
                    }
                }
            },
            Response.ErrorListener {
                if (!isFinishing) {
                    startActivity<LoginActivity>()
                    finish()
                }
            })
        queue.add(stringRequest)
    }

    override fun onStart() {
        super.onStart()
        srl_web.viewTreeObserver.addOnScrollChangedListener(scrollChangedListener)
    }

    @SuppressLint("NewApi")
    override fun onResume() {
        super.onResume()
        wv_web.onResume()
    }

    override fun onPageStarted(url: String, favicon: Bitmap?) {}

    override fun onPageFinished(url: String) {}

    override fun onPageError(
        errorCode: Int,
        description: String?,
        failingUrl: String
    ) {
    }

    override fun onDownloadRequested(
        url: String,
        suggestedFilename: String?,
        mimeType: String?,
        contentLength: Long,
        contentDisposition: String?,
        userAgent: String?
    ) {
    }

    override fun onExternalPageRequest(url: String) {}

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        wv_web.onActivityResult(requestCode, resultCode, intent)
        super.onActivityResult(requestCode, resultCode, intent)
    }

    override fun onBackPressed() {
        if (!wv_web.onBackPressed()) {
            return
        }
        super.onBackPressed()
    }

    override fun onPause() {
        wv_web.onPause()
        super.onPause()
    }

    override fun onStop() {
        srl_web.viewTreeObserver.removeOnScrollChangedListener(scrollChangedListener)
        super.onStop()
    }

    override fun onDestroy() {
        wv_web.onDestroy()
        super.onDestroy()
    }
}