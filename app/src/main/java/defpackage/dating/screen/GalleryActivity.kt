package defpackage.dating.screen

import android.app.Activity
import android.os.Bundle
import defpackage.dating.R
import kotlinx.android.synthetic.main.activity_gallery.*

class GalleryActivity : Activity() {

    @Suppress("DEPRECATION")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery)
        view_pager.adapter = PhotoAdapter(applicationContext, fragmentManager)
    }
}
