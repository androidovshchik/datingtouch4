@file:Suppress("DEPRECATION")

package defpackage.dating.screen

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import coil.api.load
import defpackage.dating.R
import kotlinx.android.synthetic.main.fragment_photo.*

class PhotoFragment : Fragment() {

    private val args: Bundle
        get() = arguments ?: Bundle()

    override fun onCreateView(inflater: LayoutInflater, root: ViewGroup?, bundle: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_photo, root, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        photo.load("file:///android_asset/photos/${args.getString("name")}")
    }

    companion object {

        @Suppress("DEPRECATION")
        fun newInstance(name: String): PhotoFragment {
            return PhotoFragment().apply {
                arguments = Bundle().apply {
                    putString("name", name)
                }
            }
        }
    }
}