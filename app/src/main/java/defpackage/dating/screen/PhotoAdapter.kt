@file:Suppress("DEPRECATION")

package defpackage.dating.screen

import android.app.FragmentManager
import android.content.Context
import androidx.legacy.app.FragmentPagerAdapter

class PhotoAdapter(context: Context, manager: FragmentManager) : FragmentPagerAdapter(manager) {

    private val photos = context.assets.list("photos")!!

    override fun getItem(position: Int) = PhotoFragment.newInstance(photos[position])

    override fun getCount() = photos.size

    override fun getPageTitle(position: Int): CharSequence? = null
}