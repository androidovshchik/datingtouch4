package defpackage.dating.screen

import android.annotation.SuppressLint
import android.app.Activity
import android.graphics.PointF
import android.os.Bundle
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import androidx.core.view.isVisible
import defpackage.dating.*
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.inputMethodManager
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import kotlin.math.roundToInt

class LoginActivity : Activity() {

    @SuppressLint("BinaryOperationInTimber")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        image.isZoomEnabled = false
        image.setOnTouchListener { _, event ->
            if (event.action != MotionEvent.ACTION_DOWN) {
                return@setOnTouchListener false
            }
            val transformCoordTouchToBitmap = image.javaClass.getDeclaredMethod(
                "transformCoordTouchToBitmap",
                Float::class.java, Float::class.java, Boolean::class.java
            )
            transformCoordTouchToBitmap.isAccessible = true
            val bitmapPoint =
                transformCoordTouchToBitmap.invoke(image, event.x, event.y, true) as PointF
            if (bitmapPoint.x in BuildConfig.LOGIN.rangeX()) {
                if (bitmapPoint.y in BuildConfig.LOGIN.rangeY()) {
                    et_login.isVisible = true
                    et_login.requestFocus()
                    inputMethodManager.showSoftInput(et_login, InputMethodManager.SHOW_IMPLICIT)
                }
            }
            if (bitmapPoint.x in BuildConfig.PASSWORD.rangeX()) {
                if (bitmapPoint.y in BuildConfig.PASSWORD.rangeY()) {
                    et_password.isVisible = true
                    et_password.requestFocus()
                    inputMethodManager.showSoftInput(et_login, InputMethodManager.SHOW_IMPLICIT)
                }
            }
            if (bitmapPoint.x in BuildConfig.SIGN_IN.rangeX()) {
                if (bitmapPoint.y in BuildConfig.SIGN_IN.rangeY()) {
                    val login = et_login.text.toString().ifBlank { null }
                    val password = et_login.text.toString().ifBlank { null }
                    if (login != null && password != null) {
                        startActivity<GalleryActivity>()
                        finish()
                    } else {
                        toast("Please fill out the form")
                    }
                }
            }
            return@setOnTouchListener false
        }
        image.post {
            val transformCoordBitmapToTouch = image.javaClass.getDeclaredMethod(
                "transformCoordBitmapToTouch",
                Float::class.java, Float::class.java
            )
            transformCoordBitmapToTouch.isAccessible = true
            et_login.apply {
                val start =
                    transformCoordBitmapToTouch.invoke(image, *BuildConfig.LOGIN.pair1()) as PointF
                val end =
                    transformCoordBitmapToTouch.invoke(image, *BuildConfig.LOGIN.pair2()) as PointF
                x = start.x
                y = start.y
                width = (end.x - start.x).roundToInt()
                height = (end.y - start.y).roundToInt()
            }
            et_password.apply {
                val start = transformCoordBitmapToTouch.invoke(
                    image,
                    *BuildConfig.PASSWORD.pair1()
                ) as PointF
                val end = transformCoordBitmapToTouch.invoke(
                    image,
                    *BuildConfig.PASSWORD.pair2()
                ) as PointF
                x = start.x
                y = start.y
                width = (end.x - start.x).roundToInt()
                height = (end.y - start.y).roundToInt()
            }
        }
    }
}
