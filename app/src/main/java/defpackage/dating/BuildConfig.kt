package defpackage.dating

fun String.rangeX(): ClosedRange<Float> {
    return split(";").let {
        require(it.size == 4)
        it[0].toFloat()..it[2].toFloat()
    }
}

fun String.rangeY(): ClosedRange<Float> {
    return split(";").let {
        require(it.size == 4)
        it[1].toFloat()..it[3].toFloat()
    }
}

fun String.pair1(): Array<Float> {
    return split(";").let {
        require(it.size == 4)
        arrayOf(it[0].toFloat(), it[1].toFloat())
    }
}

fun String.pair2(): Array<Float> {
    return split(";").let {
        require(it.size == 4)
        arrayOf(it[2].toFloat(), it[3].toFloat())
    }
}